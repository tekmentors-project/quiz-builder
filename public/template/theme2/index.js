let Theme2TemplateHandlers = function () {

    function populateView(index) {
        let a = _getStartHtml(index);
        $('#workArea .innerArea').append(a);
        let opt = _getOptions(4,index);
        $(`#list-item-${index} .template-body`).append(opt);
    }
    function updateChoiceView(id){
        console.log('checkbox');     
        getQuestionEmpty(id);
        let opt = _getOptions(4,id);
        $(`#list-item-${id} .template-body`).append(opt);
    }
    function updateBoolQuesView(id){
        console.log('boolean');
        getQuestionEmpty(id);
        let opt = _getBoolean();       
        $(`#list-item-${id} .template-body`).append(opt);
    }
    function updatecheckBoxView(id){
        console.log('checkbox');
        getQuestionEmpty(id);
        let opt = _getOptions(4,id);
        $(`#list-item-${id} .template-body`).append(opt);
        var a = document.querySelectorAll(`#list-item-${id} .form-check-input`);
        a.forEach(function(ele){
            ele.setAttribute("type","checkbox");
       })
    }
    function getQuestionEmpty(id){
        quiz.questions[id - 1].option=[];
        $(`#list-item-${id} .template-body`).empty(); 
    }
    //private method
    function _getStartHtml(id){
        let output = `<div class="template1" id="list-item-${id}" section="${id}">
        <h2 objectname="question" contenteditable="true" class="text-white mt-5">0${id}. Lorem Ipsum is simply dummy text of the printing and typesetting industry?</h2>
        <div class="template-body" >
        </div>
        <div class="btn btn-bottom"><a href="#" class="secondary float-right">Submit</a><a href="#" class="primary float-left">Prev</a> <a href="#" class="primary float-left">Next</a></div>
        <div class="selectBox">
        <select class="form-control" onchange="EditModule.selectType(this,${id})">
        <option value="multipleChoiceQuestions">Multiple Choice Questions</option>
        <option value="checkboxQusetions">Checkboxes</option>
        <option value="booleanQuestions">True/False</option>
      </select>
        </div>
       </div>`;
       return output;
     
    }
  
    function _getBox(order,inputid){
        var b = `<div class="form-check">
       <label class="form-check-label">
        <input type="radio" class="form-check-input" objectname="answer" onclick="updateJSON(event.srcElement,'template1')" name="optradio${inputid}" opt-order="${order}">
        <label class="form-check-label qw"></label>
            <div class="imageBox-${inputid} img">
            <div class = "imageArea-${order} area">
            <img src="" class="img-fluid">
              <a href="javascript:void(0)" order="${order}" class="closeParent"><i class="fa fa-times close"  objectname="close" aria-hidden="true"></i></a>
             </div>
              
            </div>
            <div class="textBox ">
            
            <span class="file position-relative ">
            <input type = "file" id="files-${inputid}" onchange="readURL(event,${inputid},${order});">
            <i class="fa fa-picture-o position-absolute"></i></span>
            </div>
            <span class="pl-2"  objectName = "option" option="${order}" contenteditable="true" quesid="${this.questionId}">Enter Your option ${order} </span>
            
            
        </label>
        <a href="#" class="delete" option="${order}"><i class="fa fa-times" aria-hidden="true"></i></a>
    </div>`;

return b;
    }
    function _getBoolean(){
        var b = `<div class="form-check">
        
            <input type="radio" class="form-check-input" name="optradio" value="true">
            <label class="form-check-label">True</label>
    </div><div class="form-check">
   
        <input type="radio" class="form-check-input" name="optradio" value="false">
        <label class="form-check-label">False</label>
    </div>`;
    return b;
    }

    function getCheckBoxes(order,inputid){
        var b = `<div class="form-check">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" objectname="answer" onclick="updateJSON(event.srcElement,'template1')" name="optradio${inputid}" opt-order="${order}">
            <div class="imageBox-${inputid} img">
            <div class = "imageArea-${order} area">
              <a href="javascript:void(0)" order="${order}"><i class="fa fa-times close"  objectname="close" aria-hidden="true"></i></a>
              </div>
              
            </div>
            <div class="textBox ">
            
            <span class="file position-relative ">
            <input type = "file" id="files-${inputid}" onchange="readURL(event,${inputid},${order});">
            <i class="fa fa-picture-o position-absolute"></i></span>
            </div>
            <span class="pl-2"  objectName = "option" option="${order}" contenteditable="true" quesid="${this.questionId}">Enter Your option ${order} </span>
            
            
        </label>
    </div>`;

return b;
    }
    function _getOptions(num,inputid){
        let output = '';
        for(let i=0;i<num;i++){
            quiz.questions[inputid - 1].option.push(
                {
                    text:`Enter your option ${i}`,
                    downloadUrl:''
                })
            output+= _getBox(i,inputid);
        }
        return output;

    }




    function getMappedUrl(){
        return new Promise((resolve, reject) => {
            const dbPath = DBPathBuilder.getUrlMapping();
            $.ajax({
                url: dbPath,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (quiz) {
                    console.log(quiz);
                    
                    resolve(quiz);
                    
                },
                error: function (error) {
                    reject(error);
                }
            });
        })
    }

    function quizView(){
        showLoader();
        getMappedUrl().then((data)=>
            {
                let urlObj = window.location.href;
                urlObj = urlObj.substr(urlObj.indexOf("-") + 1 ,urlObj.length); 
                let templateId = data[urlObj].userId;
                getQuizQuestion(templateId).then((tempDta)=>{
                    localStorage.setItem('QuizJson', JSON.stringify(tempDta))
                    view(tempDta);
                });
            });
    }
    function getQuizQuestion(templateId) {

        return new Promise((resolve, reject) => {
            const dbPath = `https://xtcore-negi.firebaseio.com/quiz/${templateId}.json`;
            $.ajax({
                url: dbPath,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (quiz) {
                    resolve(quiz);
    
                },
                error: function (error) {
                    reject(error);
                }
            });
        })
    }


    function view(data){
        $('.template1').css("margin","0");
        $('.template1').append(`<div id="carouselExampleControls" class="carousel slide caroule-sizing d-none" data-ride="carousel" data-interval="false" data-wrap="false">
        <div class="carousel-inner">`);
       
        $('#heading').text(data.templateHeading);
        $('.sub-heading').html(data.subHeading);
        $('.sub-heading').attr("contenteditable","false");
       
        let ctr = 1;
        if(data.questions !== undefined){
        data.questions.forEach((questionType,index) => {
            var a;
            var i;
            if (ctr > 1)
                a = `<div class="carousel-item" id=${index}><div class="cWrapper"><div class="innerWrap">`
            else
                a = `<div class="carousel-item active" id=${index}><div class="cWrapper"><div class="innerWrap">`
                ctr++;

            switch (questionType.type) {
                case 'mcq':
                    a += `<h2 objectname="question" class="text-white mt-5">${questionType.question}</h2><div class="template-body">`
                    for (let i in questionType.option) {
                        a += `<div class="form-check">
                       
                        <input type="radio" class="form-check-input" objectname="answer" name="optradio1" opt-order="0">
                        <label class="form-check-label">
                            <div class="imageBox-1 img">
                            <div class="imageArea-0 area">
                             </div>
                              
                            </div>
                            <div class="textBox ">
                            <span class="file position-relative ${(questionType.option[i].downloadUrl.length>0) ? '' : 'txt'} " style="background-image:url(${questionType.option[i].downloadUrl})">
                            <img src="${questionType.option[i].downloadUrl}" class="img-fluid">
                            </span>
                            </div>
                            <span class="pl-2">${questionType.option[i].text}</span>
                            
                            
                        </label>
                      
                    </div>`;
                    }

                    
                    break;
                case 'checkbox':
                a += `<h2 objectname="question" class="text-white mt-5">${questionType.question}</h2><div class="template-body">`
                for (let i in questionType.option) {
                    a += `<div class="form-check">
                   
                    <input type="checkbox" class="form-check-input" objectname="answer" name="optradio1" opt-order="0">
                    <label class="form-check-label">
                        <div class="imageBox-1 img">
                        <div class="imageArea-0 area">
                         </div>
                          
                        </div>
                        <div class="textBox ">
                        <span class="file position-relative ${(questionType.option[i].downloadUrl.length>0) ? '' : 'txt'} " style="background-image:url(${questionType.option[i].downloadUrl})">
                        <img src="${questionType.option[i].downloadUrl}" class="img-fluid">
                        </span>
                        </div>
                        <span class="pl-2">${questionType.option[i].text}</span>
                        
                        
                    </label>
                  
                </div>`;
                }

                    break;
                case 'boolean':

                    break;
                default:

            }
            a+="</div>";
          
            $('.carousel-inner').append(a);
        })

    }
    let b=`<div class="btn btn-bottom">
                    <a class="primary float-left" href="#carouselExampleControls" role="button" data-slide="prev">
    
    Prev
  </a>
  <a class="primary float-left" href="#carouselExampleControls" role="button" data-slide="next">
    
  Next
  </a><a href="javascript:void(0)" class="secondary float-right" id="submitQuiz">Submit</a></div>`;
  $('#carouselExampleControls').append(b);
  hideLoader();
    }


function showLoader(){
    $('#workArea').hide();
    $('#load').show();
}
function hideLoader(){
    $('#load').hide();
    $('#workArea').show();
}

function resultView(){
    /* console.log('dsf');
    let b = $('.carousel-item').length; */
    let a = `<div class="template1">
  
    <div class="template-body text-white">   
            <h3>Thank you for completing this quiz. Your score is below.</h3>
            
            <div class="score mt-5 mb-4">
            <p>You have Got A score Of</p>
            <p class="scoreValue">Correct No. Of Questions / Total No. questions</p>
            <p>on Your Quiz</p>
         
        </div>
        <div class="social-icons mt-5">
            <h5>You can also share your quiz:</h5>
            <ul>
                <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                <li><a href="#"><i class="fa fa-google text-white"></i></a></li>
            </ul>
        </div>
   </div>`;
    $(a).insertAfter('.innerArea');
}

var countOfCorrectAnswer = 0;
var current_progress = 0;
let currentQues = 0;
let userresult = {};
 
    function addEventHandlers() {
        $('.theme1 #cta').on("click",function(){
            console.log('cta');
            $('#list-item-0').hide().next().show();
        });
        $('#heading').on("change",function(){
            let txt = $(this).text();
            quiz.templateHeading = txt;
        })
        $('#cta').on("change",function(){
            let txt = $(this).text();
            quiz.ctaBtn = txt;
        })
        $('#workArea .innerArea').on("click","#userData",function(){
           $('#carouselExampleControls').removeClass('d-none').addClass('d-block');
           $('.template1 h1,.template1 form,.template1 p').hide();
            const DB_PATH = 'https://xtcore-negi.firebaseio.com/userdata.json';

            userresult.name = document.getElementById("first_name").value;
            userresult.email = document.getElementById("email_id").value;
            userresult.date = new Date;
        
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: DB_PATH,
                    type: 'POST',
                    data: JSON.stringify(userresult),
                    contentType: 'text/plain',
                    dataType: 'json',
                    success: function (data) {                   
                        
                        resolve(data);
                       
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
        
            });
        return false;
        });
        
        $("#workArea").on("click",".nxt .btn",function(){
            var dbvalue = JSON.parse(localStorage.getItem("QuizJson"));
    currentQues++
    var $quesId = parseInt($('.carousel-item.active').attr('id'));
 
    if ($('.carousel-item.active input[type="radio"]').is(':checked')) {
        var radioValue = parseInt($(".carousel-item.active input[type='radio']:checked").attr('index'));
        if(dbvalue.questions[$quesId].correct === radioValue){
            countOfCorrectAnswer++;
        }

        current_progress += 10;
        $("#dynamic")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress)
            .text(current_progress + "% Complete");
            current_progress = currentQues / (dbvalue.questions.length * 100);

        $('.carousel-control-next').click();
    } else if($('.carousel-item.active input[type="checkbox"]').is(':checked')){
        var check = 0;
        var checkedValue = $('.carousel-item.active .form-check-input:checked');
        for(let i = 0;i<checkedValue.length;i++) {
            check += parseInt($(checkedValue[i]).attr('index'));
            check += (i!=checkedValue.length - 1) ? ',' : '';
        } 
        if(dbvalue.questions[$quesId].correct === check)
            countOfCorrectAnswer++;
            else
            console.log('answer incorrecr');
        $('.carousel-control-next').click();
    }
    
    else {

        alert('Please select atlease one value.');

    }
    userresult.countOfCorrectAnswer = countOfCorrectAnswer;

    userresult.totalQuestion = dbvalue.questions.length;


    $('#result').text(countOfCorrectAnswer + ` out of ` + dbvalue.questions.length + ` question is correct.`);

        })
        $('#workArea').on("click",".close",function(){
            console.log('click');
            $(this).parents('.img').next().css("display","inline-block");
          let sec = parseInt($(this).parents('.img').hide().closest('.template1').attr("section"));
          let att = parseInt($(this).parent().attr("order"));
          quiz.questions[sec - 1].option[att].downloadUrl = '';
      
      });
      $('#workArea').on("click",".delete",function(){
        console.log('delete');
        $(this).parent('.form-check').remove();
        let opt = parseInt($(this).attr("option"));
        let id = parseInt($(this).closest('.template1').attr("section"));
        quiz.questions[id - 1].option[opt].splice(opt,1);
     
    
    });
    $('#workArea').on("click","#submitQuiz",function(){
        console.log('ssds');
      let b = $('.carousel-item').length;
        let a = `<div class="template1 mb-0">
  
        <div class="template-body text-white">   
                <h3>Thank you for completing this quiz. Your score is below.</h3>
                
                <div class="score mt-5 mb-4">
                <p>You have</p>
                <p>Got A score Of</p>
                <p class="scoreValue">1 / ${b}</p>
                <p>on</p>
                <p>Your Quiz</p>
            </div>
            <div class="social-icons mt-5">
                <h5>You can also share your quiz:</h5>
                <ul>
                    <li><a href="#"><i class="fa fa-facebook text-white"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter text-white"></i></a></li>
                    <li><a href="#"><i class="fa fa-google text-white"></i></a></li>
                </ul>
            </div>
       </div>`;
       $('.innerArea').html(a);
    
    });

    }




   


    return {
        updateView: populateView,
        addEventHandlers: addEventHandlers,
        changeBoolean: updateBoolQuesView,
        changeMCQ: updateChoiceView,
        changeCheckbox: updatecheckBoxView,
        getQuizView: quizView,
        getresultView : resultView,
    }
}