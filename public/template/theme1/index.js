let Theme1TemplateHandlers = function () {

    function populateView(index) {
        let a = _getStartHtml(index);
        $('#workArea .innerArea').append(a);
        let opt = _getOptions(4,index);
        $(`#list-item-${index} .cardData`).append(opt);
    }
    function updateChoiceView(id){
        console.log('checkbox');
        let opt = _getOptions(4,id);
        getQuestionEmpty(id);
        $(`#list-item-${id} .card-body .cardData`).append(opt);
    }
    function updateBoolQuesView(id){
        console.log('boolean');
        let opt = _getBoolean();
        getQuestionEmpty(id);
        $(`#list-item-${id} .card-body .cardData`).append(opt);
    }
    function updatecheckBoxView(id){
        let opt = _getOptions(4,id);
        getQuestionEmpty(id);
        $(`#list-item-${id} .card-body .cardData`).append(opt);
        var a = document.querySelectorAll(`#list-item-${id} .form-check-input`);
        a.forEach(function(ele){
            ele.setAttribute("type","checkbox");
       })
    }
    function getQuestionEmpty(id){
        $(`#list-item-${id} .card-body .cardData`).empty(); 
    }
    //private method
    function _getStartHtml(id){
        let output = `<div class="template" id="list-item-${id}" section="${id}"><div class="card" style="width: 50vw;">
        <div class = "cardHead container-fluid">
        <div class="row">
             <div class="justify-content-start d-flex col-6">
                 Question ${id}
             </div>
            
             <div class="justify-content-end d-flex col-6">
             <select class="form-control" onchange="EditModule.selectType(this,${id})">
             <option value="multipleChoiceQuestions">Multiple Choice Questions</option>
             <option value="checkboxQusetions">Checkboxes</option>
             <option value="booleanQuestions">True/False</option>
           </select>
             
             </div>
             </div>
        </div>
        <h5 class="card-title text-capitalise" objectName="question" quesid="${id}" contenteditable="true">Enter Your Question Here</h5>
         <div class="card-body">
           
           <div class="cardData">
           
           </div>
           <div class="card-foot mt-2 text-right"><button class="btn btn-primary">Next</button></div>
         </div>
        
       </div></div>`;
       return output;
     
    }
  
    function _getBox(order,inputid){
        var b = `<div class="form-check">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" objectname="answer" onclick="updateJSON(event.srcElement)" name="optradio${inputid}" opt-order="${order}">
            <div class="imageBox-${inputid} img">
            <div class = "imageArea-${order} area">
              <a href="javascript:void(0)" order="${order}"><i class="fa fa-times close"  objectname="close" aria-hidden="true"></i></a>
             </div>
              
            </div>
            <div class="textBox ">
            
            <span class="file position-relative ">
            <input type = "file" id="files-${inputid}" onchange="readURL(event,${inputid},${order});">
            <i class="fa fa-picture-o position-absolute"></i></span>
            </div>
            <span class="pl-2"  objectName = "option" option="${order}" contenteditable="true" quesid="${this.questionId}">Enter Your option ${order} </span>
            
            
        </label>
        <a href="#" class="delete" option="${order}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
    </div>`;

return b;
    }
    function _getBoolean(){
        var b = `<div class="form-check col-12">
        <label class="form-check-label">
            <input type="radio" class="form-check-input" name="optradio" value="true"><span contenteditable="true">True</span>
        </label>
    </div><div class="form-check col-12">
    <label class="form-check-label">
        <input type="radio" class="form-check-input" name="optradio" value="false"><span contenteditable="true">False</span>
    </label>
    </div>`;
    return b;
    }

    function getCheckBoxes(order,inputid){
        var b = `<div class="form-check">
        <label class="form-check-label">
        <input type="radio" class="form-check-input" objectname="answer" onclick="updateJSON(event.srcElement)" name="optradio${inputid}" opt-order="${order}">
            <div class="imageBox-${inputid} img">
            <div class = "imageArea-${order} area">
              <a href="javascript:void(0)" order="${order}"><i class="fa fa-times close"  objectname="close" aria-hidden="true"></i></a>
              </div>
              
            </div>
            <div class="textBox ">
            
            <span class="file position-relative ">
            <input type = "file" id="files-${inputid}" onchange="readURL(event,${inputid},${order});">
            <i class="fa fa-picture-o position-absolute"></i></span>
            </div>
            <span class="pl-2"  objectName = "option" option="${order}" contenteditable="true" quesid="${this.questionId}">Enter Your option ${order} </span>
            
            
        </label>
    </div>`;

return b;
    }
    function _getOptions(num,inputid){
        let output = '';
        for(let i=0;i<num;i++){
            quiz.questions[inputid - 1].option.push(
                {
                    text:`Enter your option ${i}`,
                    downloadUrl:''
                })
            output+= _getBox(i,inputid);
        }
        return output;

    }

    function abc(){
     /*    getQuizQuestion(templateId).then(function (data) {
            localStorage.setItem('QuizJson', JSON.stringify(data))
            $('#heading').text(data.templateHeading);
            $('#cta').text(data.ctaBtn);
            let ctr = 1;
            data.questions.forEach((questionType,index) => {
    
                var a;
                var i;
                if (ctr > 1)
                    a = `<div class="carousel-item" id=${index}><div class="cWrapper">`
                else
                    a = `<div class="carousel-item active" id=${index}><div class="cWrapper">`
                ctr++;
    
                switch (questionType.type) {
                    case 'mcq':
                        a += `<h3>${questionType.question}</h3>`
                        for (let i in questionType.option) {
                            a += `<div class="form-check"> 
                        <input type="radio" class="form-check-input" value="${questionType.option[i]}" index=${i} name="quesinput">
                        <label class="form-check-label" for="customCheck1">${questionType.option[i]}</label>
                      </div>`;
                        }
    
    
                        break;
                    case 'checkbox':
                    a += `<h3 index=${questionType.correct}>${questionType.question}</h3>`
                    for (let i in questionType.option) {
                        a += `<div class="form-check"> 
                    <input type="checkbox" class="form-check-input" value="${questionType.option[i]}" index=${i} name="quesinput">
                    <label class="form-check-label" for="customCheck1">${questionType.option[i]}</label>
                  </div>`;
                    }
    
                        break;
                    case 'boolean':
    
                        break;
                    default:
    
                }
    
                if (data.questions.length >= ctr) {
                    a += '<a class="btn btn-primary" href="javascript:void(0)" >Next</a></div>';
    
                } else
                    a += '<a class="btn btn-primary" href="javascript:void(0)" onclick="submitQuiz();">Submit</a></div>';
                $(a).insertBefore('#appendDiv');
            })
    
        });
    }); */
    
    }



    function getMappedUrl(){
        return new Promise((resolve, reject) => {
            const dbPath = DBPathBuilder.getUrlMapping();
            $.ajax({
                url: dbPath,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (quiz) {
                    console.log(quiz);
                    
                    resolve(quiz);
                    
                },
                error: function (error) {
                    reject(error);
                }
            });
        })
    }

    function quizView(){

        getMappedUrl().then((data)=>
            {
                let urlObj = window.location.href;
                urlObj = urlObj.substr(urlObj.indexOf("-") + 1 ,urlObj.length); 
                let templateId = data[urlObj].userId;
                getQuizQuestion(templateId).then((tempDta)=>{
                    localStorage.setItem('QuizJson', JSON.stringify(tempDta))
                    view(tempDta);
                });
            });
    }


    function resultView(){
        let a= `
        <div class="template" id="resultPage"><div class="card" style="width: 50vw;">
        <div class="cardHead container-fluid">
        <div class="row">
             <div class="justify-content-start d-flex col-6">
             <h3>Congratulations!!! </h3><br>
             </div>
            
             <div class="justify-content-end d-flex col-6">
           
             
             </div>
             </div>
        </div>
        <h5 class="card-title text-capitalise" contenteditable="true"></h5>
         <div class="card-body">
           
           <div class="cardData">
           
           <p> Your result {5} out of {10} Questions is correct</p>
        
           <br><b>Share your score<b>
                                                 <!-- Add font awesome icons -->
                                                 <a href="#" class="theme1 
                                                 fa fa-facebook" href="https://www.facebook.com/sharer
                                                 /sharer.php?u= https://corext-86e23.firebaseio.com + encodeURIComponent(document.URL)" onclick="share();"></a>

                                                 <a href="#" id="tweet" class="fa fa-twitter"></a>
                                                 <a href="#" class="fa fa-google"></a>
                                                 <a href="#" class="fa fa-linkedin"></a>
       </div></div>
        `


        $(a).insertAfter('.innerArea');
    }

    function getQuizQuestion(templateId) {

        return new Promise((resolve, reject) => {
            const dbPath = `https://xtcore-negi.firebaseio.com/quiz/${templateId}.json`;
            $.ajax({
                url: dbPath,
                type: 'GET',
                contentType: 'text/plain',
                dataType: 'json',
                success: function (quiz) {
                    resolve(quiz);
    
                },
                error: function (error) {
                    reject(error);
                }
            });
        })
    }
    function view(data){
        let a =`<div class ="template" id="started"><div class="quizForm display-none">
        <form action="/">
           <div id="carouselExampleControls" class="theme1 carousel slide caroule-sizing" data-ride="carousel" data-interval="false" data-wrap="false">
                   <div class="carousel-inner">
                     
                      
                         <div class="theme1 carousel-item" id="appendDiv">
                         <div class="userForm cWrapper">
                         
                         <div class="innerWrap">
                             <form action="/" name="userForm">
                                 <fieldset class="form-group">
                                 <label for="first_name">Enter your Name</label>
                                 <input type="text" class="form-control" id="first_name" name="first_name">
                                 </fieldset>
                                 <fieldset class="form-group">
                                 <label for="email_id">Email Id</label>
                                 <input type="text" class="form-control" id="email_id" name="email_id">
                                 </fieldset>
                                 <a class="btn btn-primary" id="userData" href="javascript:void(0)">Submit</a>
                                  </form>
                                  </div>
                               </div>
                               
                           </div>
                           <div class="carousel-item">

                           <div class="quizResult  cWrapper">
                            <div class="innerWrap">
                               <h1>Congratulations!!!</h1>
                                               
                             <h2 id="result"></h2>
                           </div>
                           </div>
                         </div>
                   </div>
                   <a class="carousel-control-prev d-none" href="#carouselExampleControls" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                   </a>
                   <a class="carousel-control-next d-none" href="#carouselExampleControls" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                   </a>
                 </div>
           </form>
           </div></div>`;
           $('#workArea .innerArea').append(a);
        $('#heading').text(data.templateHeading);
        $('#cta').text(data.ctaBtn);
        let ctr = 1;
        if(data.questions !== undefined){
        data.questions.forEach((questionType,index) => {    

            var a;
            var i;
            if (ctr > 1)
                a = `<div class="carousel-item" id=${index}><div class="card" style="width: 50vw;">
                <div class="cardHead container-fluid">
                <div class="row">
                     <div class="justify-content-start d-flex col-6">Question ${index}</div></div></div>`
            else
                a = `<div class="carousel-item active" id=${index}><div class="card" style="width: 50vw;">
                <div class="cardHead container-fluid">
                <div class="row">
                     <div class="justify-content-start d-flex col-6">Question ${index}</div></div></div>`
            ctr++;

            switch (questionType.type) {
                case 'mcq':
                    a += `<h5 class="card-title text-capitalise">${questionType.question}</h5>    
                        <div class="card-body">
           



                        
                        <div class="cardData">`
                    for (let i in questionType.option) {
                        a += `<div class="form-check"> 
                    <input type="radio" class="form-check-input" value="${i}" index=${i} name="quesinput">
                   
                        <img src = "${(questionType.option[i].downloadUrl.length > 0 ) ? questionType.option[i].downloadUrl : ''}" class="img-fluid">
                    
                    <label class="form-check-label" for="customCheck1">${questionType.option[i].text}</label>
                  </div>`;
                    }


                    break;
                case 'checkbox':
                a += `<h3 index=${questionType.correct}>${questionType.question}</h3>`
                for (let i in questionType.option) {
                    a += `<div class="form-check"> 
                    <input type="checkbox" class="form-check-input" value="${questionType.option[i].text}" index=${i} name="quesinput">
                   
                        <img src = "${(questionType.option[i].downloadUrl.length > 0 ) ? questionType.option[i].downloadUrl : ''}" class="img-fluid">
                    
                
                <label class="form-check-label" for="customCheck1">${questionType.option[i].text}</label>
              </div>`;
                }

                    break;
                case 'boolean':

                    break;
                default:

            }

            if (data.questions.length >= ctr) {
                a += '</div></div><div class="nxt"><a class="btn btn-primary" href="javascript:void(0)">Next</a></div></div></div></div>';

            } else
                a += '</div></div><div class="nxt"><a class="btn btn-primary" href="javascript:void(0)" onclick="submitQuiz();">Submit</a></div></div></div></div>';
            $(a).insertBefore('#appendDiv');
        })

    }
    }
    var countOfCorrectAnswer = 0;
var current_progress = 0;
let currentQues = 0;
let userresult = {};
 
    function addEventHandlers() {
        $('.theme1 #cta').on("click",function(){
            console.log('cta');
            $('#list-item-0').hide().next().show();
        });
        $('#heading').on("change",function(){
            let txt = $(this).text();
            quiz.templateHeading = txt;
        })
        $('#cta').on("change",function(){
            let txt = $(this).text();
            quiz.ctaBtn = txt;
        })
        $('#workArea').on("click","#userData",function(){
            const DB_PATH = 'https://xtcore-negi.firebaseio.com/userdata.json';

            userresult.name = document.getElementById("first_name").value;
            userresult.email = document.getElementById("email_id").value;
            userresult.date = new Date;
        
            return new Promise((resolve, reject) => {
                $.ajax({
                    url: DB_PATH,
                    type: 'POST',
                    data: JSON.stringify(userresult),
                    contentType: 'text/plain',
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $('.carousel-control-next').click();
                        resolve(data);
                       
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
        
            });
        
        });
        $("#workArea").on("click",".nxt .btn",function(){
            var dbvalue = JSON.parse(localStorage.getItem("QuizJson"));
    currentQues++
    var $quesId = parseInt($('.carousel-item.active').attr('id'));
 
    if ($('.carousel-item.active input[type="radio"]').is(':checked')) {
        var radioValue = parseInt($(".carousel-item.active input[type='radio']:checked").attr('index'));
        if(dbvalue.questions[$quesId].correct === radioValue){
            countOfCorrectAnswer++;
        }

        current_progress += 10;
        $("#dynamic")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress)
            .text(current_progress + "% Complete");
            current_progress = currentQues / (dbvalue.questions.length * 100);

        $('.carousel-control-next').click();
    } else if($('.carousel-item.active input[type="checkbox"]').is(':checked')){
        var check = 0;
        var checkedValue = $('.carousel-item.active .form-check-input:checked');
        for(let i = 0;i<checkedValue.length;i++) {
            check += parseInt($(checkedValue[i]).attr('index'));
            check += (i!=checkedValue.length - 1) ? ',' : '';
        } 
        if(dbvalue.questions[$quesId].correct === check)
            countOfCorrectAnswer++;
            else
            console.log('answer incorrecr');
        $('.carousel-control-next').click();
    }
    
    else {

        alert('Please select atlease one value.');

    }
    userresult.countOfCorrectAnswer = countOfCorrectAnswer;

    userresult.totalQuestion = dbvalue.questions.length;


    $('#result').text(countOfCorrectAnswer + ` out of ` + dbvalue.questions.length + ` question is correct.`);

        })

        

    }




   


    return {
        updateView: populateView,
        addEventHandlers: addEventHandlers,
        changeBoolean: updateBoolQuesView,
        changeMCQ: updateChoiceView,
        changeCheckbox: updatecheckBoxView,
        getQuizView: quizView,
        getresultView: resultView
    }
}