const config = {
    apiKey: "AIzaSyAAA17PYzdiwCKYbSn3DPrOK5TSYl302uw",
    authDomain: "xtcore-negi.firebaseapp.com",
    databaseURL: "https://xtcore-negi.firebaseio.com",
    projectId: "xtcore-negi",
    storageBucket: "xtcore-negi.appspot.com",
    messagingSenderId: "376681254139"
};
firebase.initializeApp(config);

var quiz;
$(document).ready(function () {
    intializeCurrentTemplate();
});

function intializeCurrentTemplate(){
    console.log('current User');
    initializeHeader();
    $('body').addClass('theme2');
    initializeWorkspace('theme2');
    initializeQuiz(); 
    initializeLeftSideBar();
    setTimeout(function(){		
    initializeDummyData();		
  },100);
    
}
function initializeHeader(){
    addHeaderEventListeners();
    updateHeaderView();
}
function addHeaderEventListeners(){
    //firebase auth check and update
}
function updateHeaderView(){
    // chedck if user is logged in or not and update view accordingly
}
function initializeQuiz(){
    quiz = new QuizWiz('newQuiz');
}

function getresultView(){
    quiz = new QuizWiz('newQuiz');
   
}

function initializeDummyData(){ 
    console.log("!!!")	
  for(let i=0;i<2;i++) {
    let index = createNewQuestion();      	 
    updateLeftSideBarView(index);	 
    updateWorkspace(index);		
     }		
    activeTemplate.script.getresultView(); 		
 }


function initializeWorkspace(tid){

      if (TEMPLATES[tid]) {
        $("#workArea").load(TEMPLATES[tid].html, function () {
           console.log('updateView');
           activeTemplate = TEMPLATES[tid];
           quiz.type = 'mcq';
           quiz.templateHeading = TEMPLATES[tid].heading;
           quiz.subHeading = TEMPLATES[tid].subHeading;
        });
    }
}

function updateWorkspace(order){
    
    activeTemplate.script.addEventHandlers();
    activeTemplate.script.updateView(order);
}

var initializeLeftSideBar = function () {
    leftSideBarListeners();
}
function leftSideBarListeners() {
    $(".list-group-item-action ").on("click",function(e) {
        e.preventDefault();
        
        var position = $($(this).attr("href")).offset().top;
    
        $("body, html").animate({
            scrollTop: position
        } /* speed */ );
    });
    $("#addQuestion").on("click",function (event) {      
            event.preventDefault();
            let index = createNewQuestion();      
            updateLeftSideBarView(index);
            updateWorkspace(index);
    });
}
function updateLeftSideBarView(order) {
    $(`<a class="list-group-item list-group-item-action mb-2" data-order="${order}" href="#list-item-${order}">Question-${order}</a>`).insertBefore('#addQuestion');
}

function createNewQuestion(){
    var ques = new Option('mcq');
    var a = quiz.questions.push(ques);
    return a;
}



function updateJSON(element,area) {
    let $id = parseInt(event.srcElement.closest('.' + area).getAttribute('section'));
    let objName = event.srcElement.getAttribute('objectname');
    let $quizz = quiz.questions[$id - 1];
    switch (objName) {
        case 'question': {
            $quizz.question = event.srcElement.innerText;
            break;
        }
        case 'option': {
            let $opt = parseInt(event.srcElement.getAttribute('option'));
            $quizz.option[$opt].text = event.srcElement.innerText;
            break;
        }
        case 'subHeading':{
            quiz.subHeading = event.srcElement.innerHTML;
            break;
        }
        case 'heading': {
            quiz.setHeading(event.srcElement.innerText);
            break;
        }
        case 'btn': {
            quiz.setCta(event.srcElement.innerText);
            break;
        }
        case 'answer': {
            if($quizz.type=='mcq'){
                $quizz.correct = parseInt(event.srcElement.getAttribute('opt-order'));
            }else if($quizz.type=='checkbox'){
                let val = '';
                var checkedValue = $('#list-item-' + $id + ' input[type="checkbox"]:checked');
                for(let i = 0;i<checkedValue.length;i++) {
                    val += $(checkedValue[i]).attr('opt-order');
                    val += (i!=checkedValue.length - 1) ? ',' : '';
                  };
                  $quizz.correct = val;
            }   
        }

    }
    console.log(quiz);
}

function publishUrl() {
    saveJson(quiz).then((data) => {
        hideLoader();  
        let url = window.location.href;
url = (url.substr(0,url.indexOf('#')).length > 0) ? url.substr(0,url.indexOf('#')) + 'quiz-' : url + 'quiz-'
console.log(url);
       
        $('#url').val(url).attr('publishId',data.name);
        $('#publishModal').modal('show');
    });
}
$('#nameQuiz').keyup(function(){
    let url = window.location.href;
url = (url.substr(0,url.indexOf('#')).length > 0) ? url.substr(0,url.indexOf('#')) + 'quiz-' : url + 'quiz-'
    let newUrl = url + $("#nameQuiz").val();
        $('#url').val(newUrl)
})
function saveJson() {
    getLoader();
    var DB_PATH = 'https://xtcore-negi.firebaseio.com/quiz.json';
    return new Promise((resolve) => {
        $.ajax({
            url: DB_PATH,
            type: 'POST',
            data: JSON.stringify(quiz),
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                resolve(data);
            },
            error: function (error) {
                return error;
            }
        });
    });
}

function saveUrl(){

    const dbPath = DBPathBuilder.getUrlMapping();
    let obj = {};
    let id = $('#url').attr('publishId');
    let name = $('#nameQuiz').val();
    obj[name] = {
        userId: id
    }
return new Promise((resolve, reject) => {
    $.ajax({
        url: dbPath,
        type: 'PUT',
        data:JSON.stringify(obj),
        contentType: 'text/plain',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            resolve(data);

        },
        error: function (error) {
            reject(error);
        }
    });
});
}

var EditModule = (function() {
  
    return {
        selectType: function(a,id){
            var x = (a.value || a.options[a.selectedIndex].value);  //crossbrowser solution =)
            EditModule[x](id);
        },
        multipleChoiceQuestions: function(id) {
           quiz.questions[id - 1].type="mcq";
           activeTemplate.script.changeMCQ(id); 
        },
        checkboxQusetions: function(id){
           quiz.questions[id - 1].type="checkbox"
           activeTemplate.script.changeCheckbox(id); 
        },
        booleanQuestions:function(id){
            quiz.questions[id - 1].type="boolean";
            activeTemplate.script.changeBoolean(id);          
        }
    };
}());




function getLoader(){
    $('#load').show();
}
function hideLoader(){
    $('#load').hide();
}

function copyToClipboard() {
    var copyText = document.getElementById("url");
    copyText.select();
    document.execCommand("copy");
    alert("Copied the text: " + copyText.value);
  }



  /* $('#workArea').on("click",".close",function(){
      console.log('click');
      $(this).parents('.img').next().css("display","inline-block");
    let sec = parseInt($(this).parents('.img').hide().closest('.template').attr("section"));
    let att = parseInt($(this).parent().attr("order"));
    quiz.questions[sec - 1].option[att].downloadURL = '';

});
$('#workArea').on("click",".delete",function(){
    console.log('delete');
    $(this).parent('.form-check').remove();
    let opt = parseInt($(this).attr("option"));
    let id = parseInt($(this).closest('.template').attr("section"));
    quiz.questions[id - 1].option[opt].splice(opt,1);
 
}); */

  function readURL(e,inputId,index){
     /*    e.stopPropagation();
        e.preventDefault(); */
        var storageRef = firebase.storage().ref();
        var fileName = e.target.files[0].name;
        var file = e.target.files[0];
        if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (el) {
                $(`.imageBox-${inputId} .imageArea-${index} img`).attr('src', el.target.result);
            }
    
            reader.readAsDataURL(e.target.files[0]);
        }
        var metadata = {
            'contentType': file.type
        };
        var uploadTask = storageRef.child('quiz').child(file.name).put(file, metadata);

        uploadTask.on('state_changed', null, function(error) {
            alert('51 Upload failed:' + error);
        }, function() {
            //alert('Uploaded', uploadTask.snapshot.totalBytes, 'bytes.');
            //console.log(uploadTask.snapshot);
            uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                console.log('File available at', downloadURL);
                quiz.questions[inputId - 1].option[index].downloadUrl = downloadURL;
                setImage(downloadURL,inputId,index)
                var length = downloadURL.lastIndexOf("%");
                var resource = downloadURL.substring(length, downloadURL.length);
                console.log('short', resource);
                download = downloadURL;
            });
        });

    };

    function setImage(url,id,imgId){
     // $(`.imageBox-${id} .imageArea-${imgId}`).css("background-image","url('" + url +"')");
        $(`#list-item-${id} .imageArea-${imgId}`).parent().css("display","inline-block").next('.textBox').hide();
    }
