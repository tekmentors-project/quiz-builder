
firebase.auth().onAuthStateChanged(user => {
    if (!user) {

        window.location = 'quizLogin.html'; //If User is not logged in, redirect to login page

    } else {

        var name = user.email.split('@');
        name = name[0];
        console.log(name[0]);

        $('.welcome').append(`<a class="nav-link" href="#"><b>Welcome ${name} </b></a>`);

        var playersRef = firebase.database().ref("quiz/");

        playersRef.orderByChild("email").on("child_added", function (data) {
            // <div class="overlay-box">
            //                                 <div class="d-flex justify-content-center align-items-center h-100">
            //                                     <button class="btn bg-white px-2 rounded" onclick="isUserLoggedIn();">Use Template</button>
            //                                 </div>
            //                             </div>
            if (data.val().email === user.email) {
                console.log(data.val().templateName);
                console.log(typeof (data.val().email));
                console.log(data.val().email === user.email);
                console.log(data.val().questions.length);
                console.log(data.key);
                $('.row').append(`<div class="col-md-6 col-sm-12 mb-5">
                                    <div class="card shadow">
                                        <div class="box position-relative">
                                            <img class="card-img-top" src="images/quizenter.gif" alt="Card image cap">
                                            
                                        </div>
                                        <div class="card-body">
                                            <b>Quiz Template:</b> ${data.val().templateName}<br>
                                            <b>Quiz Name:</b><a href=${data.key}> ${data.val().templateHeading} </a><br>
                                            <b>Total no of question:</b> ${data.val().questions.length}<br>
                                            <b>Date: </b>${data.val().date}
                                        </div>

                                    </div>
                                    </div>`);

            }
        });


    }


});


function logout() {
    if (firebase.auth().currentUser) {
        console.log(firebase.auth().currentUser);
        firebase.auth().signOut();
        window.location = 'home.html';
    }
}