 var DBPathBuilder = (function() {
    const BASE_PATH = 'https://xtcore-negi.firebaseio.com';
    return {
        
        getTemplatesPath : function(){
            return `${BASE_PATH}/quiz.json`
        },
        getQuestionsPath : function (templateid){
            return `${BASE_PATH}/quiz/${templateId}.json`
        },
        getUrlMapping : function(){
            return `${BASE_PATH}/urlMapping.json`
        }
      
    }
}());



