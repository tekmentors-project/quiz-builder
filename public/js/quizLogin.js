window.onload = function () {
    
   
  
    const txtEmail = document.getElementById('email');
    const txtPassword = document.getElementById('password');
    const googleSignin = document.getElementById('googleSignin');
    const facebookSignin = document.getElementById('facebookSignin');

    signin.addEventListener('click', e => {

        const email = txtEmail.value;
        const pass = txtPassword.value;
        const auth = firebase.auth();

        //Sign in
        console.log(email);
        console.log(auth);

        const promise = auth.signInWithEmailAndPassword(email, pass).then(() => {
            
        }).catch(function (error) {
            console.log(error.code);
            console.log(error.message);
        });

    });

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;

            console.log(displayName + "|" + email + "|" + emailVerified + "|" + photoURL + "|" + isAnonymous + "|" + uid + "|" + providerData);
            window.location = 'home.html';
			//window.history.back();
            
        }
    });

    var provider = new firebase.auth.GoogleAuthProvider();

    googleSignin.addEventListener('click', e => {
        console.log("google");
        const auth = firebase.auth();

        auth.signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token)
            console.log(user)
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;

            console.log(errorCode)
            console.log(errorMessage)
        });
    });


    // var provider = new firebase.auth.FacebookAuthProvider();

    // facebookSignin.addEventListener('click', e => {
    //     console.log("facebook");
    //     firebase.auth().signInWithPopup(provider)

    //         .then(function (result) {
    //             var token = result.credential.accessToken;
    //             var user = result.user;

    //             console.log(token)
    //             console.log(user)
    //         }).catch(function (error) {
    //             console.log(error.code);
    //             console.log(error.message);
    //         });
    // });

};



