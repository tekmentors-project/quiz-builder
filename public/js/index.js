

firebase.auth().onAuthStateChanged(user => {
    if (!user) {

        $('#login').append('<a class="nav-link btn bg-orange text-white p-1" href="quizLogin.html">log in</a>');

    } else {
        var name = user.email.split('@');
        name = name[0];
        console.log(name[0]);

        $('.welcome').append(`<a class="nav-link" href="#"><b>Welcome ${name} </b></a>`);

        $('#logout').append(`<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            <i class="fa fa-user-o" aria-hidden="true"></i>
        </a>
        <div class="dropdown-menu">           
            <a class="dropdown-item" href="account.html">Account</a>
            <a class="dropdown-item" href="myQuiz.html">My Quiz</a>
            <a class="dropdown-item" href="javascript:void(0)" onclick="logout();">Log Out</a>
        </div>`);

        console.log(user.email);

        var playersRef = firebase.database().ref("quiz/");

        playersRef.orderByChild("email").on("child_added", function (data) {

            if (data.val().email === user.email) {
                console.log(data.val().email);
                console.log(typeof (data.val().email));
                console.log(data.val().email === user.email);

            }
        });


    }
});

function logout() {
    if (firebase.auth().currentUser) {
        console.log(firebase.auth().currentUser);
        firebase.auth().signOut();
        window.location = 'home.html';
    }
}

function isUserLoggedIn() {

    firebase.auth().onAuthStateChanged(user => {
        if (!user) {
            window.location = 'quizLogin.html'; //If User is not logged in, redirect to login page
        } else {
            window.location = 'templates.html'


        }
    });

}




