
const TEMPLATES = {}
TEMPLATES['theme1'] = {
    name:'Theme-1',
    html: `template/theme1/index.html`,
    src:`template/theme1/index.js`,
    heading:"Enter Your Data",
    subHeading:`Enter Your User `,
    script: Theme1TemplateHandlers()
};
TEMPLATES['theme2'] = {
    name:'Theme-2',
    html: `template/theme2/index.html`,
    heading:"Let's Start",
    subHeading:`Enter Your User nmae <br>
    to start the quiz`,
    script: Theme2TemplateHandlers()
};

//quizWiz
class QuizWiz {
    constructor(name) {
        this.questions=[];
        this.templateName = name;
        this.templateHeading = "MY main heading";
        this.subHeading = "";
        this.ctaBtn  = "start quiz";
        this.mode = 'teacher';
    }
    setHeading(heading){
        this.templateHeading = heading;
    }
    setCta(ctaTxt){
        this.ctaBtn = ctaTxt;
    }
   
  }

//Questions
  class Questions{
    constructor(type){
      this.type = type;
      this.question = 'Enter Your Question';
      this.correct = '';
    }
 }

//mcq
class Option extends Questions{
    constructor(type){
      super(type);
      this.option = [];
    }   
}