window.onload = function () {

    const txtEmail = document.getElementById('email');
    const txtPassword = document.getElementById('password');
    const txtconfirmpassword = document.getElementById('confirmpassword');
    const signUp = document.getElementById('signUp');

    signUp.addEventListener('click', e => {

        const email = txtEmail.value;
        const password = txtPassword.value;
        const confirmpassword = txtconfirmpassword.value;


        if (txtPassword.value != txtconfirmpassword.value) {

            let element = document.getElementById("alertClass");
            element.classList.remove("d-none");
            document.getElementById("signUpAlert").innerHTML = "Passwords Don't Match";


        } else {
            const auth = firebase.auth();
            //Sign Up
            console.log(email);
            console.log(auth);

            auth.createUserWithEmailAndPassword(email, password)
                .then(() => {
                    console.log('Sign up Sucessfully');

                })
                .catch(function (error) {
                    console.log(error.code);
                    console.log(error.message);
                    let element = document.getElementById("alertClass");
                    element.classList.remove("d-none");
                    document.getElementById("signUpAlert").innerHTML = error.message;
                });
        }
    });

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;

            console.log(displayName + "|" + email + "|" + emailVerified + "|" + photoURL + "|" + isAnonymous + "|" + uid + "|" + providerData);
			window.history.back();
            //window.location = 'landing-page.html';
        }
    });
}