$(document).ready(function () {
    intializeCurrentTemplate();
});
function intializeCurrentTemplate(){
    initializeWorkspace("theme2");
}
function initializeWorkspace(tid){
    if (TEMPLATES[tid]) {
        $("#workArea").addClass(tid);
        $("#workArea").load(TEMPLATES[tid].html, function () {
         $('#workArea h1,#workArea a,#workarea p').attr("contenteditable","false");
           activeTemplate = TEMPLATES[tid];
           updateWorkspace();
           $(".innerArea").prop("onkeyup", null).off("keyup");
        });

    }
}
function updateWorkspace(){
    
    activeTemplate.script.addEventHandlers();
    activeTemplate.script.getQuizView();
}

function getAllTemplates() {
    const dbPath = 'https://xtcore-negi.firebaseio.com/quiz.json';
    return new Promise((resolve, reject) => {
        $.ajax({
            url: dbPath,
            type: 'GET',
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                resolve(data);

            },
            error: function (error) {
                reject(error);
            }
        });
    });
}





/* var getAllData = getAllTemplates().then((data) => {
    const templateIds = [];


   

  let templateId = '-LLTzvcAdK5v04nCnAny';
      templateId = templateId.substr(templateId.indexOf("=") + 1 ,templateId.length); 
    
    getQuizQuestion(templateId).then(function (data) {
        localStorage.setItem('QuizJson', JSON.stringify(data))
        $('#heading').text(data.templateHeading);
        $('#cta').text(data.ctaBtn);
        let ctr = 1;
        data.questions.forEach((questionType,index) => {

            var a;
            var i;
            if (ctr > 1)
                a = `<div class="carousel-item" id=${index}>`
            else
                a = `<div class="carousel-item active" id=${index}>`
            ctr++;

            switch (questionType.type) {
                case 'mcq':
                    a += `<h3>${questionType.question}</h3>`
                    for (let i in questionType.option) {
                        a += `<div class="form-check"> 
                    <input type="radio" class="form-check-input" value="${questionType.option[i]}" index=${i} name="quesinput">
                    <label class="form-check-label" for="customCheck1">${questionType.option[i]}</label>
                  </div>`;
                    }


                    break;
                case 'checkbox':
                a += `<h3 index=${questionType.correct}>${questionType.question}</h3>`
                for (let i in questionType.option) {
                    a += `<div class="form-check"> 
                <input type="checkbox" class="form-check-input" value="${questionType.option[i]}" index=${i} name="quesinput">
                <label class="form-check-label" for="customCheck1">${questionType.option[i]}</label>
              </div>`;
                }

                    break;
                case 'boolean':

                    break;
                default:

            }

            if (data.questions.length >= ctr) {
                a += '<a class="btn btn-primary" href="javascript:void(0)" onclick="nxtQuestion();">Next</a></div>';

            } else
                a += '<a class="btn btn-primary" href="javascript:void(0)" onclick="submitQuiz();">Submit</a></div>';
            $(a).insertBefore('#appendDiv');
        })

    });
});

function startQuiz() {
    $('.mainFlow').hide().siblings('.quizForm').show();

}
function submitQuiz() {
    nxtQuestion();
    $('.progress').hide();
}

function userData() {
    const DB_PATH = 'https://xtcore-negi.firebaseio.com/userdata.json';

    userresult.name = document.getElementById("first_name").value;
    userresult.email = document.getElementById("email_id").value;
    userresult.date = new Date;

    return new Promise((resolve, reject) => {
        $.ajax({
            url: DB_PATH,
            type: 'POST',
            data: JSON.stringify(userresult),
            contentType: 'text/plain',
            dataType: 'json',
            success: function (data) {
                console.log(data);

                resolve(data);
                $('.carousel-control-next').click();
            },
            error: function (error) {
                console.log(error)
            }
        });

    });

}
 */

/* function nxtQuestion(){
    console.log('abc');
} */